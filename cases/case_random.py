import random
import string


def case_int():
    print(random.randrange(0, 10))
    # >>> 8 Или другое случайное число из 0..9

    print(random.randint(0, 10))
    # >>> 8 Или другое случайное число из 0..10


def case_choices():
    print(random.choice(string.ascii_lowercase))
    # >>> o Или другая случайная буква из a..z

    print(random.choices(string.ascii_lowercase, k=2))
    # >>> ['y', 'b'] Или другие 2 случайные буквы из a..z


if __name__ == '__main__':
    case_int()
    case_choices()