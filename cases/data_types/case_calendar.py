import calendar


def case_calendar():
    print(calendar.monthrange(2022, 1))
    # >>> (5, 31)

    print(list(calendar.Calendar().yeardayscalendar(2022)))
    # >>> [[[[0, 0, 0, 0, 0, 1, 2], [3, 4, 5, 6, 7, 8, 9]...

    print(calendar.isleap(2022))
    # >>> False

    print(calendar.weekday(2022, 1, 1))
    # >>> 5


if __name__ == '__main__':
    case_calendar()