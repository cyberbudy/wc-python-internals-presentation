from collections import Counter


def case_most_common():
    print(Counter('abracadabra').most_common(3))
    # >>> [('a', 5), ('b', 2), ('r', 2)]


def case_elements():
    c = Counter(a=4, b=2, c=0, d=-2)
    print(list(c.elements()))
    # >>> ['a', 'a', 'a', 'a', 'b', 'b']

    c['a'] += 1
    print(c['a'])
    # >>> 5




if __name__ == '__main__':
    case_most_common()
    case_elements()