from collections import defaultdict


def case_int_value():
    d = defaultdict(int)
    seq = 'aaabbbddcdcdcd'

    for char in seq:
        d[char] += 1

    print(d)
    # >>> {'a': 3, 'b': 3, 'd': 5, 'c': 3}


def case_list_value():
    d = defaultdict(list)
    seq = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]

    for colour, number in seq:
        d[colour].append(number)

    print(d)
    # >>> {'yellow': [1, 3], 'blue': [2, 4], 'red': [1]}


if __name__ == '__main__':
    case_int_value()
    case_list_value()