import zoneinfo as zi
from datetime import datetime, timedelta, timezone


def case_dst():
    dt = datetime(2020, 10, 31, 12, tzinfo=zi.ZoneInfo("America/Los_Angeles"))
    print(dt)
    # >>> 2020-10-31 12:00:00-07:00
    print(dt + timedelta(days=1))
    # >>> 2020-11-01 12:00:00-08:00


def real_utc():
    # Есть временная зона
    print(datetime.now(timezone.utc))
    # >>> 2022-02-09 12:08:33+00:00

    # Нет временной зоны
    print(datetime.utcnow())
    # >>> 2022-02-09 12:08:33

    # Если системное время не UTC, можно получить ошибочный результат
    print(datetime.utcnow().astimezone(timezone.utc))
    # >>> 2022-02-09 10:08:33+00:00



if __name__ == '__main__':
    case_dst()
    real_utc()