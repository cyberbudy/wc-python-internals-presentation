from collections import namedtuple

Point = namedtuple('Point', ['x', 'y'])


def case_namedtuple():
    print(Point(1, 2))
    # >>> Point(x=1, y=2)

    print(Point(1, 2)._asdict())
    # >>> {'x': 1, 'y': 2}


def case_tuple_arg(point: Point):
    print(point.x, point.y)

    # >>> 2, 1


if __name__ == '__main__':
    case_namedtuple()
    case_tuple_arg(Point(y=1, x=2))