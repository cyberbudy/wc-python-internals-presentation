from copy import deepcopy, copy


def case_compound_copy():
    d = {'a': [1, 2, 3], 'b': ''}
    d_copy = d.copy()
    d_copy_ = copy(d)
    d_deepcopy = deepcopy(d)

    d['a'].append(4)
    d['b'] += '1'

    print(d)
    # >>> {'a': [1, 2, 3, 4], 'b': '1'}
    print(d_copy)
    # >>> {'a': [1, 2, 3, 4], 'b': ''}
    print(d_copy_)
    # >>> {'a': [1, 2, 3, 4], 'b': ''}
    print(d_deepcopy)
    # >>> {'a': [1, 2, 3], 'b': ''}



if __name__ == '__main__':
    case_compound_copy()