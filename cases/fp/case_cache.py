from functools import cached_property, lru_cache


class A:
    @cached_property
    def property_hits_once(self) -> bool:
        print("Hey, missed cache")
        return True


def call_property():
    instance = A()
    [instance.property_hits_once for _ in range(10)]
    # Напишет только раз, все остальные вызовы из кеша
    # >>> Hey, missed cache


@lru_cache(maxsize=4)
def cached_func(param: int):
    print(param + 1)
    return param + 1


def call_func():
    for i in range(3):
        for j in range(4):
            cached_func(j)
    # Выведет только один раз, так как кеш кеширует 4 значения
    # >>> 1 2 3 4



if __name__ == '__main__':
    call_property()
    call_func()