from itertools import chain


def case_chain_lists():
    a = [1, 2, 3]
    b = (3, 4, 5)

    print(list(chain(a, b)))
    # >>> 1, 2, 3, 3, 4, 5


if __name__ == '__main__':
    case_chain_lists()