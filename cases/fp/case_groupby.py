from itertools import groupby


def case_groupby_queryset():
    # Сортировка крайне важна!
    grouped = groupby(
        queryset.order_by('catalog_id', 'resolved_product_variant_id'),
        key=lambda x: (x.catalog_id, x.resolved_product_variant_id)
    )