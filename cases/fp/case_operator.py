from functools import reduse
from operator import or_

from django.db.models import Q


def case_queryset():
    filters = {
        'a': 1,
        'b': 2,
        'c': 3,
        'd': 4
    }
    # Применить | между всеми фильтрами
    filter = reduce(or_, [Q(**{k: v}) for k, v in filters.items()])
    Model.objects.filter(filter)