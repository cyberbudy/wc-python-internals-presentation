from typing import Any
from functools import partial


redis = {}
CACHE_KEY = 'redis::key'


def cache_getter(cache, key: Any, default: Any) -> Any:
    return cache.get(key, default)


def cache_setter(cache, key: Any, value: Any) -> Any:
    cache[key] = value

    return value


def cache_processor(getter, setter):
    value = getter(key=CACHE_KEY, default=None)

    if value is None:
        setter(key=CACHE_KEY, value=10)

    print(getter(key=CACHE_KEY, default=None))
    # >>> 10


redis_getter = partial(cache_getter, cache=redis)
redis_setter = partial(cache_setter, cache=redis)


if __name__ == '__main__':
    cache_processor(redis_getter, redis_setter)