from itertools import product


def case_product():
    print(list(product('ABCD', 'xy')))
    # >>> [('A', 'x'), ('A', 'y'), ('B', 'x'), ('B', 'y'), ('C', 'x'), ('C', 'y'), ('D', 'x'), ('D', 'y')]

    print(list(product(range(2), repeat=3)))
    # >>> [(0, 0, 0), (0, 0, 1), (0, 1, 0), (0, 1, 1), (1, 0, 0), (1, 0, 1), (1, 1, 0), (1, 1, 1)]


if __name__ == '__main__':
    case_product()