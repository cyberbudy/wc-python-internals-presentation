from itertools import starmap


def case_starmap():
    seq = [(2,5), (3,2), (10,3)]
    print(list(starmap(pow, seq)))

    # >>> 32 9 1000


if __name__ == '__main__':
    case_starmap()
