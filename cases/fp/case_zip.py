from itertools import zip_longest


def case_zip():
    print(list(zip_longest('ABCD', 'xy', fillvalue='+')))
    # >>> [('A', 'x'), ('B', 'y'), ('C', '+'), ('D', '+')]

    print(list(zip('ABCD', 'xy')))
    # >>> [('A', 'x'), ('B', 'y')]


if __name__ == '__main__':
    case_zip()
