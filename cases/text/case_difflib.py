import difflib


def case_ndiff():
    """Выводит разницу между строками, аля гит"""
    diff = difflib.ndiff(
        'one\ntwo\nthree\n'.splitlines(keepends=True),
        'ore\ntree\nemu\n'.splitlines(keepends=True)
    )
    print(''.join(diff), end="")

def case_close_matches():
    """Возвращает наиболее похожие слова"""
    print(difflib.get_close_matches('appel', ['ape', 'apple', 'peach', 'puppy']))
    # >>> ['apple', 'ape']

def case_matcher():
    """Возвращает коэфициент - на сколько совпадают строки"""
    s = difflib.SequenceMatcher(None, "abcd", "bcde")
    print(s.ratio())
    # >>> 0.75

if __name__ == '__main__':
    case_ndiff()
    case_matcher()