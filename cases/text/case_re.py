import re


def case_search():
    m = re.search('(?<=abc)def', 'abcdef')
    print(m.group(0))
    # >>> def

def case_findall():
    print(re.findall(r'\bf[a-z]*', 'which foot or hand fell fastest'))
    # >>> ['foot', 'fell', 'fastest']


if __name__ == '__main__':
    case_search()
    case_findall()