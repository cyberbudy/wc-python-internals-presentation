import string
import random

VOCABULARY = string.ascii_letters + string.digits
ALL_VOCABULARY = string.ascii_letters + string.digits + string.punctuation


def random_string_safe_url(length: int = 10) -> str:
    return ''.join(random.choices(VOCABULARY, k=length))


def random_string(length: int = 10) -> str:
    return ''.join(random.choices(ALL_VOCABULARY, k=length))


if __name__ == '__main__':
    print(random_string_safe_url())
    print(random_string())
