from typing import List
import textwrap as tw


def case_wrap(multiline: str, max_width: int = 10) -> List[str]:
    """Делит длинную строку на список коротких строк"""
    return tw.wrap(multiline, width=max_width)


def case_shorten(line: str, max_width: int = 10) -> str:
    """Обрезает длинную строку до указанной длины + префикс"""
    return tw.shorten(line, width=max_width, placeholder="...")


if __name__ == '__main__':
    print(case_wrap('as ' * 100))
    print(case_shorten('as ' * 10))